








def ask_ok(prompt, retries=4, message="yes"):
    while True:
        ok = input(prompt)
        if ok in ('y', 'ye', 'yes'):
            return True;
        if ok in ('n', 'no', 'nop', 'nope'):
            return False
        retries  = retries -1 
        if retries < 0:
            print("Maximum number of retries exceeded")
        print(message)
        
        
#ans = ask_ok("Do you really want to quit?")        
#ans = ask_ok("Do you really want to quit?",2)
ans = ask_ok("Do you really want to quit?",2,"Please provide Yes or No only")
print("answer is "+ str(ans))


while True:
    x = int(input("Enter a number : "))
    if x < 0:
        print("\n" + str(x) + " is negative")
    elif x == 0:
        print("\n" + str(x) + " is neither positive nor negative")
    elif x > 0 :
        print("\n" + str(x) + " is positive")
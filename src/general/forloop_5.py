



#basic list and iteration
alist = ["apple", "orange", "grapes", "mango"]
for item in alist:
    print(item, len(item))

#appending item to the list
alist.append("raspberry");
print(alist)


#loop over a slice to add an item to the original list
for fruit in alist[:]:
    if len(fruit) > 6 :
        alist.insert(0, fruit)
print(alist)



#range function
for i in range(5):
    print(i,end=' ')
    
print("\n")    
for i in range(5,10):
    print(i, end="  ")

print("\n")       
for i in range(0,10,2):
    print(i, end="  ")


#using a range and len together
print("\n")       
nlist = ['Mary', 'had', 'a','little' ,'lamp']
for i in range(len(nlist)) :
    print(i, nlist[i]) 




word = "Jurrasic Park 2"


print(word.startswith("J"))
print(word.startswith("x"))
print(word.endswith("2"))
print(word.endswith("a"))
print("r" in word)
print(word.islower())
print(word.isupper())
print(word.isalpha())
print(word.isalnum())
print(word.isdigit())
print(word.istitle()) 


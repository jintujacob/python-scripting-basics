from nltk.book import *

"""
[len[w] for w in text5 ]  is equialent to for loop below

for w in text5:
    len[w]

"""


"""Distribution of the word length in a text """
[len[w] for w in text1 ] 


""" Frequency distribution of the above """
fdist0 = FreqDist([len(w) for w in text1])
print(fdist0.keys())


print(fdist0.items())

print(fdist0.max())

print(fdist0.freq(3))

""" what % of a 3 letter words makes up a book? """

fdist = FreqDist([len(w) for w in text1])
fdist.max()
fdist.freq(3) * 100  #""" what % of a 3 letter words makes up a book? """ 

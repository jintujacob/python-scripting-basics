

name = "monty"
print(name[0])
print(name[4])

#join the items of a list to form a SENTENCE_PATTERN

alist = ['Call', 'me', 'Ishmael', '.', 'The', 'family', 'of', 'Dashwood', 'had', 'long', 'been', 'settled', 'in', 'Sussex', '.', 'I', 'am', 'on', 'leave', 'today']

spacer = " "
outstring = spacer.join(alist)
print(outstring)


tokens = set(alist)
tokens = sorted(tokens)
print("\ntokens - ")
print(tokens)   #words starting with capital letters first.

print(tokens[-2:])
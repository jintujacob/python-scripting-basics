from nltk.book import *

"""
find words with more than 15 CHARACTERS

let P be this proerty,
    P(w) is true if and only if  w is more than 15 chars long
    the set of all w such that w is an element of V (the vocabulary) 
    - and w has property P.”

a. {w | w ∈ V & P(w)}            -- V is vocabulary
b. [w for w in V if p(w)]        -- Python expression

"""

V = set(text1)
long_words = [w for w in V if len(w) > 15]
print(sorted(long_words))
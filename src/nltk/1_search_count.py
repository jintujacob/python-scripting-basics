
from __future__ import division
from nltk.book import *




text1.concordance("monstrous")
text1.similar("monstrous")

print("\n\n-len() function usage")
length = len(text3)
print(length)


print("\n\n-count() function usage")
print(text3.count("still"))

print("\n\n-percentage of the text taken up by a word")
print(100 * text1.count("a") / len(text1))

print("\n\n-lexical diversity")
print(len(text3)/len(set(text3)))


def lexical_diversity(text):
    return len(text)/len(set(text))

def percentage(count,total):
    return 100 * count / total

print("\n\n-percentage - function call")
print(percentage(text1.count("the"), len(text1)))

print("lexical diversity")
print(lexical_diversity(text6))

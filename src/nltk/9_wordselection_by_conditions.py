from nltk.book import *

#show a list of words that ends with 'ness' in text1 corpus in alphabetical order
alist = [w for w in set(text1) if w.endswith('ness')]
alist = sorted(alist)


#count the list of words that contains 'ful' in text1 corpus
blist = [word for word in set(text1) if "ful" in word]
len(blist)


#filter and show the first 10 tiles  [ie.  Hello, World, India ...]in alphabetical order
# in text6 corpus

clist = [item for item in set(text6) if item.istitle()] 
clist = sorted(clist) 
clist[0:10] 


#show an item if it is a digit in sent7 corpus
dlist = sorted([item for item in sent7 if item.isdigit()])


"""  Other excercises """
sorted([w for w in set(text7) if '-' in w and 'index' in w])
sorted([wd for wd in set(text3) if wd.istitle() and len(wd) > 10])
sorted([w for w in set(sent7) if not w.islower()])
sorted([t for t in set(text2) if 'cie' in t or 'cei' in t])

